def print_one(arg1):
  print "arg1: %r" %arg1
def print_two(arg1, arg2):
  print "arg1: %r, arg2: %r" % (arg1, arg2)
print_one("First!")
print_two("Hello", "World")