def add(a, b):
  print "ADDING %d + %d" % (a, b)
  return a + b

def substract(a, b):
  print "SUBSTRACTING %d - %d" % (a, b)
  return a - b

def multiply(a, b):
  print "MULTYPLY %d * %d" % (a, b)
  return a * b

def devide(a, b):
  print "ADDING %d / %d" % (a, b)
  return a / b

print "Let's do some math with just functions!"

age = add(40, 3)
height = substract (200, 12)
weight = multiply (8, 11)
iq = devide (200, 2)

print "Age: %d, Height: %d, Weight: %d, IQ: %d" % (age, height, weight, iq)
print "Here is a puzzle"

what = add(age, substract(height, multiply(weight, devide(iq, 2))))

print "That becomes: ", what, "Can you do that by hand?"
